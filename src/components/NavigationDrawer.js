import React, { Component } from 'react';
import Drawer from 'react-native-drawer';
import { Actions, DefaultRenderer } from 'react-native-router-flux';
import MainSideMenu from './MainSideMenu';

export default class NavigationDrawer extends Component {
    componentDidMount() {
        Actions.refresh({ key: 'drawer', ref: this.refs.navigation });
    }

    render() {
        const state = this.props.navigationState;
        const children = state.children;
        return (
            <Drawer
                ref="navigation"
                open={state.open}
                onOpen={() => Actions.refresh({ key: state.key, open: true })}
                onClose={() => Actions.refresh({ key: state.key, open: false })}
                type="static"
                content={<MainSideMenu />}
                tapToClose
                openDrawerOffset={0.2}
                panCloseMask={0.2}
                negotiatePan
                tweenHandler={(ratio) => ({
                    main: { opacity: Math.max(0.54, 1 - ratio) }
                })}
                styles={styles.drawerStyle}
            >
                <DefaultRenderer
                    navigationState={children[0]}
                    onNavigate={this.props.onNavigate}
                />
            </Drawer>
        );
    }
}

const styles = {
    drawerStyle: {
        shadowColor: '#000000',
        shadowOpacity: 0.8,
        shadowRadius: 3
    }
};
