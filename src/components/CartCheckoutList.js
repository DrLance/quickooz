import React from 'react';
import { FlatList } from 'react-native';
import { View, Left, Right, Text } from 'native-base';
import CartItem from './commons/CartItem';

const CartCheckoutList = (props) => {
  return (
    <View>
      <View style={styles.marketStyle}>
        <Left style={{ left: 10 }}>
          <Text style={styles.textMarketStyle}>Niks Store</Text>
        </Left>
        <Right>
          <Text style={styles.textPriceStyle}>{props.totalPrice.toFixed(2)}</Text>
        </Right>
      </View>
      <View style={styles.deliveryStyle}>
        <Text
          style={styles.textDeliveryStyle}
        >
          Next delivery:Sat, 1 May 2016 19:00 - 20:00
            </Text>
        <Right>
          <Text style={styles.textDeliveryStyle}>Free</Text>
        </Right>
      </View>
      <View>
        <FlatList
          data={props.dataSource}
          renderItem={({ item }) => (
            <CartItem {...item} />
          )}
          keyExtractor={item => item.id}
        />
      </View>
    </View>
  );
};

const styles = {
  marketStyle: {
    height: 75,
    backgroundColor: '#007993',
    flexDirection: 'row',
  },
  deliveryStyle: {
    height: 30,
    backgroundColor: '#006276',
    flexDirection: 'row',
    flex: 1
  },
  textDeliveryStyle: {
    fontSize: 12,
    color: '#fff',
    alignSelf: 'center',
    left: 15
  },
  textMarketStyle: {
    color: '#fff'
  },
  textPriceStyle: {
    padding: 5,
    color: '#fff',
    backgroundColor: '#006E83',
    alignSelf: 'center'
  }
};

export default CartCheckoutList;
