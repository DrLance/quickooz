import React, { Component } from 'react';
import { connect } from 'react-redux';
import { _ } from 'lodash';
import { Container, Content, Text, Body, Button, Footer, Grid, Col } from 'native-base';
import { BackHeader } from './commons';
import { addGood, refreshPrice } from '../actions';
import CardCheckoutList from './CartCheckoutList';

class Cart extends Component {
  constructor() {
    super();
    this.state = {
      sumPrice: 0,
      newStateChoice: ''
    };
  }
  componentWillMount() {
    this.groupByChoice(this.props.goodChoice);
  }

  componentWillReceiveProps(nextProps, nextState) {
    this.groupByChoice(nextProps.goodCartChoice);
    while (this.props.goodChoice.length > 0) {
      this.props.goodChoice.pop();
    }

    nextProps.goodCartChoice.forEach(v => {
      this.props.goodChoice.push({
        id: v.id,
        name: v.name,
        price: v.price,
        picture: v.picture,
        md5: v.md5
      });
    });
  }

  onPressCheckout() {
    this.props.refreshPrice(this.state.sumPrice);
  }

  groupByChoice(value) {
    let totalPrice = 0;
    const newChoice = _.chain(value)
      .groupBy('md5')
      .map((v, i) => {
        return {
          id: i,
          md5: i,
          name: _.get(_.find(v, 'name'), 'name'),
          price: _.get(_.find(v, 'price'), 'price'),
          picture: _.get(_.find(v, 'picture'), 'picture'),
          cnt: v.length,
          sum_price: parseFloat(_.get(_.find(v, 'price'), 'price')).toFixed(2) * v.length
        };
      })
      .value();

    _.forEach(newChoice, v => {
      totalPrice += parseFloat(v.sum_price);
    });
    this.setState({ sumPrice: totalPrice, newStateChoice: newChoice });
  }

  renderFooter() {
    if (this.props.goodCartChoice.length !== 0) {
      return (
        <Footer style={{ alignItems: 'center', backgroundColor: '#FF7C00' }}>
          <Body style={{ justifyContent: 'center' }}>
            <Button full transparent onPress={this.onPressCheckout.bind(this)}>
              <Text style={{ color: '#fff' }}>CHECKOUT</Text>
            </Button>
          </Body>
        </Footer>
      );
    }
  }

  renderContainer() {
    if (this.props.goodChoice.length !== 0) {
      return (
        <CardCheckoutList dataSource={this.state.newStateChoice} totalPrice={this.state.sumPrice} />
      );
    }

    return (
      <Grid>
        <Col style={{ height: 500, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ alignSelf: 'center' }}>Your Cart is Empty</Text>
          <Text>Your navent added any items to your cart</Text>
          <Button full style={{ alignSelf: 'center', backgroundColor: '#FF7C00' }}>
            <Text>Start Shooping</Text>
          </Button>
        </Col>
      </Grid>
    );
  }

  render() {
    return (
      <Container>
        <BackHeader />
        <Content>
          {this.renderContainer()}
        </Content>
        {this.renderFooter()}
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    goodsList: state.goodsList,
    goodChoice: state.goodChoice,
    goodCartChoice: state.goodCart.goodChoice
  };
};

export default connect(mapStateToProps, { refreshPrice, addGood })(Cart);
