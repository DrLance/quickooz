import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Icon } from 'native-base';
import images from '../config/images';

class MainSideMenu extends Component {

  render() {
    return (
      <View style={styles.menuStyle}>
        <View style={styles.avatarStyle}>
          <Image
            style={styles.logo}
            source={images.testImg}
          />
          <Text style={styles.textAvatarStyle}>FistName LastName</Text>
          <Text style={styles.textAvatarStyle}>Description of the name</Text>
        </View>
        <View style={styles.menuItemsStyle}>
          <Icon name='md-home' />
          <Text
            style={styles.textStyle}
            onPress={() => {
              Actions.get('drawer').ref.toggle();
              Actions.listStore({ type: 'replace' });
            }}
          >Home</Text>
        </View>
        <View style={styles.menuItemsStyle}>
          <Icon name='md-person' size={26} />
          <Text
            style={styles.textStyle}
            onPress={() => {
              Actions.get('drawer').ref.toggle();
              Actions.account();
            }}
          >Account</Text>
        </View>
        <View style={styles.menuItemsStyle}>
          <Icon name='md-archive' size={26} />
          <Text
            style={styles.textStyle}
            onPress={() => {
              Actions.get('drawer').ref.toggle();
              Actions.orderhistory({ type: 'replace' });
            }}
          >Order History</Text>
        </View>
        <View style={styles.menuItemsStyle}>
          <Icon name='md-information-circle' size={26} />
          <Text
            style={styles.textStyle}
            onPress={() => {
              Actions.get('drawer').ref.toggle();
              Actions.contact();
            }}
          >Contact Us</Text>
        </View>
        <View style={styles.menuItemsStyle} />
        <View style={styles.menuItemsStyle} />
      </View>
    );
  }

}

const styles = {
  logo: {
    width: 100,
    height: 100,
    borderRadius: 50
  },
  menuStyle: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    flexDirection: 'column',
    position: 'relative',
    paddingLeft: 10,
    flex: 1
  },
  menuItemsStyle: {
    flex: 1,
    flexDirection: 'row'
  },
  textStyle: {
    fontSize: 18,
    color: '#000',
    paddingLeft: 25
  },
  avatarStyle: {
    width: ('100%'),
    height: 200,
    alignItems: 'flex-start',
  },
  textAvatarStyle: {
    fontSize: 14,
    color: '#000',
    paddingLeft: 25,
    paddingTop: 5
  }
};

export default connect(null)(MainSideMenu);
