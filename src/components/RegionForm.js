import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { connect } from 'react-redux';
import { searchPostalcode } from '../actions';
import { Input, Button } from './commons';
import images from '../config/images';

class RegionForm extends Component {

  onPressSearch() {
    const { postalcode } = this.props;

    this.props.searchPostalcode({ postalcode });
  }

  render() {
    return (
      <View style={styles.containerStyle}>
      <Image
        style={styles.thumbnailStyle}
        source={images.logo}
      />
        <Input value={this.props.postalcode} placeholder="Enter Postcode e.g Hp11" />
        <View style={styles.btnSearchStyle}>
          <Button onPress={this.onPressSearch.bind(this)}>
            Search
          </Button>
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ searchPostal }) => {
  const { postalcode } = searchPostal;

  return { postalcode };
};

const styles = {
  containerStyle: {
    flexDirection: 'column',
    paddingTop: 85,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative'
  },
  buttonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 100,
    height: 35,
    borderRadius: 3,
    backgroundColor: '#09a925',
    marginLeft: 5,
    marginRight: 5
  },
  textStyle: {
    fontSize: 18,
    color: '#FFFFFF'
  },
  thumbnailStyle: {
    height: 125,
    width: 125
  },
  btnSearchStyle: {
    paddingTop: 20
  }
};

export default connect(mapStateToProps, { searchPostalcode })(RegionForm);
