import React, { Component } from 'react';
import { View, Text, Image, Linking, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { loginUser } from '../actions';
import { Input, Button } from './commons';
import images from '../config/images';

class StartForm extends Component {

  onOpenURL() {
    Linking.openURL('http://google.com');
  }
  onOpenSignUp() {
    Linking.openURL('http://www.quickooz.com/signup');
  }

  onLoginPress() {
    const { email, password } = this.props;

    this.props.loginUser({ email, password });
  }

  render() {
    const {
      thumbnailStyle,
      textStyle,
      containerStyle,
      loginStyle,
      btnUrlStyle,
      lineStyle,
      textStyleSign
    } = styles;
    return (
      <View style={containerStyle}>
        <Image
          style={thumbnailStyle}
          source={images.logo}
        />
        <View style={loginStyle}>
          <Input
            placeholder="Email"
            value={this.props.email}
          />
          <Input
            secureTextEntry
            placeholder="Password"
            value={this.props.password}
          />
        </View>
        <TouchableOpacity onPress={this.onOpenURL.bind(this)}>
          <Text style={btnUrlStyle}>
            Fogot your login details?
          </Text>
        </TouchableOpacity>
        <Button
          onPress={this.onLoginPress.bind(this)}
        >
          Login
        </Button>
        <Text style={lineStyle} />
        <View style={{ flexDirection: 'row' }}>
          <Text style={textStyle}>
            Dont have an account?
          </Text>
          <TouchableOpacity onPres={this.onOpenSignUp.bind(this)}>
            <Text style={textStyleSign}>
              Sign up here!
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = {
  thumbnailStyle: {
    height: 125,
    width: 125
  },
  textStyle: {
    paddingTop: 20,
    fontSize: 12
  },
  containerStyle: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginStyle: {
    paddingTop: 10,
    width: 250,
    flexDirection: 'column'
  },
  btnUrlStyle: {
    paddingTop: 10,
    paddingLeft: 135,
    fontSize: 10,
    textDecorationLine: 'underline',
    paddingBottom: 20
  },
  lineStyle: {
    paddingTop: 60,
    borderBottomWidth: 0.5,
    width: 350
  },
  textStyleSign: {
    paddingTop: 20,
    fontSize: 12,
    textDecorationLine: 'underline',
    paddingLeft: 5
  }
};

const mapStateToProps = ({ auth }) => {
  const { email, password } = auth;

  return { email, password };
};

export default connect(mapStateToProps, { loginUser })(StartForm);
