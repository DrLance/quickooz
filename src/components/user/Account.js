import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Input } from '../commons';
import images from '../../config/images';

class Account extends Component {
  render() {
    return (
      <View style={styles.containerStyle}>
        <View style={styles.avatarStyle}>
          <Image
            style={styles.thumbnailStyle}
            source={images.testImg}
          />
        </View>
        <View style={styles.loginStyle}>
          <Input
            placeholder="First name"
            value={this.props.email}
          />
          <Input
            placeholder="Last name"
            value={this.props.email}
          />
          <Input
            placeholder="Email"
            value={this.props.email}
          />
          <Input
            secureTextEntry
            placeholder="Password"
            value={this.props.password}
          />
          <Input
            placeholder="Phone number"
            value={this.props.email}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    paddingTop: 50,
    alignItems: 'center'
  },
  thumbnailStyle: {
    height: 150,
    width: 150,
    borderRadius: 75
  },
  loginStyle: {
    paddingTop: 20,
    width: ('90%'),
    flexDirection: 'column'
  },
  avatarStyle: {
    backgroundColor: '#E9E9E9',
    width: ('100%'),
    alignItems: 'center',
  }
};

export default Account;
