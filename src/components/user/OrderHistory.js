import React, { Component } from 'react';
import { Container, View, Text } from 'native-base';
import { BackHeader } from '../commons';

class OrderHistory extends Component {
  render() {
    return (
      <Container>
        <BackHeader />
        <View style={styles.containerStyle}>
          <Text>Order History is Empty</Text>
        </View>
      </Container>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  imgStyle: {
    height: ('100%'),
    width: ('100%')
  }
};

export default OrderHistory;
