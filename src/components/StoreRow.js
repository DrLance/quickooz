import React, { Component } from 'react';
import { View, Text, Image, TouchableWithoutFeedback, } from 'react-native';
import { Actions } from 'react-native-router-flux';

class StoreRow extends Component {
	onRowPress() {
		Actions.listGoods({ name: this.props.name.first, picture: this.props.picture.large, });
	}

	render() {
		return (
			<TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
				<View style={styles.container}>
					<Image
					source={{
						uri: this.props.picture.large
					}}
					style={styles.photo}
					/>
					<Text style={styles.textStyle}>
						{this.props.name.first}
					</Text>
					<Text style={styles.textPriceStyle}>
						{this.props.name.last}
					</Text>
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

const styles = {
	container: {
		width: 100,
		alignSelf: 'flex-start',
		flexDirection: 'column',
		paddingTop: 5,
	},
	textStyle: {
		fontSize: 12,
		paddingTop: 5,
	},
	textPriceStyle: {
		fontSize: 8
	},
	photo: {
		width: 100,
		height: 100
	},
};

export default StoreRow;
