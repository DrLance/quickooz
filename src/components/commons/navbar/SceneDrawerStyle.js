import React from 'react';
import { Scene } from 'react-native-router-flux';

const SceneDrawerStyle = ({ props }) => {
  return (
    <Scene
      key={props.key}
      navigationBarStyle={styles.mainSceneStyle}
      onRight={props.onRight}
      rightTitle={props.rightTitle}
      renderRightButton={props.renderRightButton}
    >
      {props.children}
    </Scene >
  );
};

const styles = {
  startFormStyle: {
    backgroundColor: '#FFFFFF'
  },
  mainSceneStyle: {
    backgroundColor: '#fff'
  }
};

export { SceneDrawerStyle };
