import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';

const SaveButtonNav = () => {
  return (
    <TouchableOpacity
      style={[{
        height: 25,
        width: ('100%'),
        flexDirection: 'row',
        alignItems: 'center',
      }]}
      onPress={() => { Actions.drawer(); }}
    >
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Icon
          name="check"
          size={25}
          color={'#000'}
        />
        <Text>Save</Text>
      </View>
    </TouchableOpacity>
  );
};

export { SaveButtonNav };

