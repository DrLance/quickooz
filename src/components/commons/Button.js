  import React from 'react';
  import { Text, TouchableOpacity } from 'react-native';

  const Button = ({ onPress, children }) => {
    const { buttonStyle, textStyle } = styles;

    return (
      <TouchableOpacity onPress={onPress} style={buttonStyle}>
        <Text style={textStyle}>
          {children}
        </Text>
      </TouchableOpacity>
    );
  };

  const styles = {
    textStyle: {
      fontSize: 18,
      color: '#FFFFFF'
    },
    buttonStyle: {
      alignItems: 'center',
      justifyContent: 'center',
      width: 100,
      height: 35,
      borderRadius: 3,
      backgroundColor: '#09a925',
      marginLeft: 5,
      marginRight: 5
    }
  };

  export { Button };
