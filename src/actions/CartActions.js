import { Actions } from 'react-native-router-flux';
import { REFRESH_PRICE } from './types';

export const refreshPrice = (value) => {
  return (dispatch) => {
    dispatch({ type: REFRESH_PRICE, payload: value });
    Actions.cartCheckoutForm();
  };
};
