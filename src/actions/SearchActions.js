import { Actions } from 'react-native-router-flux';
import {
  SEARCH_POSTALCODE
} from './types';

export const searchPostalcode = ({ postalcode }) => {
  return (dispatch) => {
    dispatch({ type: SEARCH_POSTALCODE, payload: postalcode });
    Actions.drawer();
  };
};
