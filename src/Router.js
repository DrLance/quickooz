import React, { Component } from 'react';
import { Scene, Router } from 'react-native-router-flux';
import { SceneDrawerStyle, SaveButtonNav, CancelButtonNav } from './components/commons/navbar';
import StartForm from './components/StartForm';
import RegionForm from './components/RegionForm';
import StoreList from './components/StoreList';
import NavigationDrawer from './components/NavigationDrawer';
import Account from './components/user/Account';
import OrderHistory from './components/user/OrderHistory';
import ContactInfo from './components/user/ContactInfo';
import GoodsList from './components/GoodsList';
import GoodCart from './components/GoodCart';
import GoodsPromoView from './components/GoodsPromoView';
import Cart from './components/Cart';
import CartCheckoutForm from './components/CartCheckoutForm';
import SearchForm from './components/SearchForm';
import Confirm from './components/Confirm';

class RouterComponent extends Component {
  render() {
    return (
      <Router key="mainRoute" sceneStyle={styles.mainSceneStyle}>
        <Router key="auth">
          <Scene key="login" component={StartForm} hideNavBar />
          <Scene key="region" component={RegionForm} hideNavBar />
        </Router>
        <Router key="drawer" component={NavigationDrawer} open={false}>
          <SceneDrawerStyle key="main" hideNavBar>
            <Scene key="listStore" component={StoreList} title="Store List" />
            <Scene key="listGoods" component={GoodsList} title="Goods List" />
            <Scene key="goodCart" component={GoodCart} />
            <Scene key="goodsPromo" component={GoodsPromoView} />
            <Scene
              key="account"
              component={Account}
              title="Account detail"
              renderRightButton={SaveButtonNav}
              renderBackButton={CancelButtonNav}
            />
            <Scene key="contact" component={ContactInfo} title="Contacts" />
            <Scene key="orderhistory" component={OrderHistory} title="Order History" />
            <Scene key="cart" component={Cart} title="Cart" />
            <Scene key="cartCheckoutForm" component={CartCheckoutForm} title="Cart" />
            <Scene key="searchForm" component={SearchForm} title="Search" />
            <Scene key="confirm" component={Confirm} title="Confirm Order" />
          </SceneDrawerStyle>
        </Router>
      </Router>
    );
  }
}

const styles = {
  mainSceneStyle: {
    backgroundColor: '#fff'
  }
};

export default RouterComponent;
