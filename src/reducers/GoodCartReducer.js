import {
  INCREMENT_GOOD,
  DECREMENT_GOOD,
  ADD_GOOD,
  REMOVE_GOOD,
  REFRESH_PRICE
} from '../actions/types';

const INITIAL_STATE = {
  countGood: 1,
  goodChoice: [],
  sumPrice: '1',
  isRemove: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case INCREMENT_GOOD:
      return { ...state, countGood: action.payload };
    case DECREMENT_GOOD:
      return { ...state, countGood: action.payload };
    case ADD_GOOD:
      return { ...state, goodChoice: action.payload };
    case REMOVE_GOOD:
      return { ...state, goodChoice: action.payload };
    case REFRESH_PRICE:
      return { ...state, sumPrice: action.payload };
    default:
      return state;
  }
};
