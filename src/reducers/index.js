import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer.js';
import RegionSearchReducer from './RegionSearchReducer';
import StoreListReducer from './StoreListReducer';
import GoodsListReducer from './GoodsListReducer';
import GoodsCategoryReducer from './GoodsCategoryReducer';
import GoodCartReducer from './GoodCartReducer';
import GoodChoiceReducer from './GoodChoiceReducer';

export default combineReducers({
  auth: AuthReducer,
  searchPostal: RegionSearchReducer,
  storeList: StoreListReducer,
  goodsList: GoodsListReducer,
  goodsCategoryList: GoodsCategoryReducer,
  goodCart: GoodCartReducer,
  goodChoice: GoodChoiceReducer
});
