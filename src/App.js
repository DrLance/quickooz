import React, { Component } from 'react';
import { Provider } from 'react-redux';
import configureStore from './config/store';
import Router from './Router';

class App extends Component {

  constructor() {
    super();
    this.state = {
      isLoading: false,
      store: configureStore(() => this.setState({ isLoading: false })),
    };
  }

  render() {
    return (
      <Provider store={this.state.store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
