import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducer from '../reducers';

export default function configureStore(onCompletion: () => void): any {  
  
  const store = createStore(reducer, {}, applyMiddleware(ReduxThunk));
  

  return store;
}
